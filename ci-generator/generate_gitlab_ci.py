#!/usr/bin/env python
from src.gitlab_ci_generator import GitlabCiGenerator

if __name__ == "__main__":
    GitlabCiGenerator().print()
